from flask import Flask, request
from models import generate_model
from csv import writer
import pickle
import numpy as np


#Author: Nir Sherf, 2020
#Written for Ofek 324, Data Science Course
#Description: Demo for a simple ML microservice
#ML model lib - scikit-learn
#Server lib - Flask

app = Flask(__name__)


def load_model(pickle_file_path):
    with open(pickle_file_path, 'rb') as file:
        loaded_model = pickle.load(file)
    return loaded_model


def parse_house_from_json(house_as_json):
    return np.asarray(list(house_as_json.values()), dtype=np.float64)


def add_line_to_csv(arr, path):
    with open(path, 'a+', newline='') as file:
        csv_writer = writer(file)
        csv_writer.writerow(arr)


@app.route('/', methods=['GET', 'POST'])
def homepage():
    return 'ground control to major Tom'


@app.route('/query_example', methods=['GET', 'POST'])
def get_name_from_query():
    if request.method == 'GET':
        name = request.args.get('name')
        if name is not None:
            return 'Hello ' + name
        else:
            return "pass name in query"
    return "pass name Via GET request"


@app.route('/predict_house_price', methods=['GET', 'POST'])
def predict_house_price():
    if request.method == 'GET':
        return 'pass house properties via Post request'
    elif request.method == 'POST':
        house_properties = parse_house_from_json(request.get_json())
        price = model.predict([house_properties])
        tagged_prediction = list(house_properties)
        tagged_prediction.extend(price)
        add_line_to_csv(tagged_prediction, 'data/predictions/boston_price_predictions.csv')
        return str(price[0])
    return 'hello'


@app.route('/train_model', methods=['GET', 'POST'])
def train_model():
    if request.method == 'GET':
        password = request.args.get('password')
        if password is None:
            return 'send password via query'
        elif password == '1234':
            generate_model()
            global model
            model = load_model('models/file_example.pkl')
            return 'trained model'
        else:
            return 'password incorrect'
    return 'send password via query'


if __name__ == '__main__':
    app.run('0.0.0.0', 8000)
