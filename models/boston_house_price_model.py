from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
import pickle


def generate_model():
    x, y = load_boston(return_X_y=True)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=42)
    rf_model = RandomForestRegressor(n_estimators=1000).fit(x_train, y_train)

    with open('models/file_example.pkl', 'wb') as file:
        pickle.dump(rf_model, file)
